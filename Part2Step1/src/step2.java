import java.util.Scanner;

public class step2 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Input a string:");
		String userIn = input.nextLine();
		
		int points = pointCounter(userIn);
		
		System.out.println("Codepoint count = " + points);

	}
	
	public static int pointCounter(String input) {
		int points = 0;
		for(int i=0; i<input.length(); i++) {
			points++;
		}
		return points;
	}
	
	public static char charAt(String input, int index) {
		return input.charAt(index);
		
	}
	
	public static int vowelCount(String input) {
		int counter = 0;
		
		for(int i=0; i<input.length(); i++) {
			if("aeiouAEIOU".indexOf(input.charAt(i))!= -1) {
				counter++;
			}
		}
		return counter;
	}

}
