import java.util.Scanner;

public class step1 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		System.out.println("Input a string:");
		String userIn = input.nextLine();
		
		
		
		System.out.println("The Middle character in the string is: "  + middleChar(userIn));
	
	}
	
	public static String middleChar(String input) {
		String middle = "";
		if(input.length()%2 == 0) {
			middle = input.substring((input.length()/2)-1, (input.length()/2)+1);
		}
		else {
			middle = input.substring((input.length()/2), (input.length()/2)+1);
		}
		
		return middle;
		
	}
	
	public static double average(double num1, double num2, double num3) {
		double average = num1 + num2 + num3;
		average = average/3;
		return average;
	}
	
	public static double smallestNum (double num1, double num2, double num3) {
		double smallest = num1;
		
		if (smallest >  num2 || smallest > num3) {
			if(num2 > num3) {
				smallest = num3;
			}
			else {
				smallest = num2;
			}
		}
		
		return smallest;
		
	}
	
	public static String returnName (String first, String last) {
		return first + " " + last;
	}
	
	

}
